## Snark

Companion CLI to [snark.qnzl.co](https://gitlab.com/qnzl/snark), a site as a placeholder for blocked sites. Snark helps you realize that you are going to sites you've deemed wasteful and suggest activities away from the computer, in a somewhat snarky way. 

### Pre-requisites

If you have `node` already installed:

> _Note:_ `snark` needs sudo permissions to edit the `hosts` file which is why `sudo` is needed when installing the CLI

```bash
npm install
sudo npm link
```

If you not, you will need to add `snark.exe` to your PATH, in the case of Windows, or move the executuable to `/usr/bin` in the case of Linux and macOS

### Usage

`snark -h`

> _Note:_ Currently `snark` only sets records to 140.82.3.56, code for that page can be found [here](https://gitlab.com/qnzl/snark)

To block site:

`snark block <site>`

To unblock site:

`snark unblock <site>`

To show all sites blocked:

`snark list`
