#!/usr/bin/env node

const hostile = require('hostile')
const yargs = require('yargs')
const pify = require('pify')

// Include coimmon subdomains
const getAllDomains = (domain) => {
  // Remove all protocols
  domain = domain.replace(/([A-z]+\:\/\/)/, '')

  let addlAddress
  if (domain.slice(0, 4) === 'www.') {
    addlAddress = domain.slice(4)
  } else {
    addlAddress = `www.${domain}`
  }

  return [ domain, addlAddress ]
}

yargs
  .command('block [site]', 'Block site across your computer', (cmd) => {
    cmd
      .positional('site', {
        describe: 'site to block',
        required: true
      })
      .help('h')
  }, async (argv) => {
    const domains = getAllDomains(argv.site)

    domains.forEach((domain) => {
      hostile.set('140.82.3.56', domain)
    })

    process.stderr.write(`${argv.site} blocked\n`)
  })
  .command('unblock [site]', 'Unblock site across your computer', (cmd) => {
    cmd
      .positional('site', {
        describe: 'site to block',
        required: true
      })
      .help('h')
  }, async (argv) => {
    let addlAddress
    const domains = getAllDomains(argv.site)

    domains.forEach((domain) => {
      hostile.remove('140.82.3.56', domain)
    })

    process.stderr.write(`${argv.site} unblocked\n`)
  })
  .command('list', 'List all sites blocked', (cmd) => {
    cmd
      .help('h')
  }, (argv) => {
    let lines = hostile.get(false)

    // Get addresses only once
    const uniqSites = new Set()
    lines = lines.map(([ ip, name ]) => {
      const cleanedName = name.replace('www.', '')

      if (uniqSites.has(cleanedName)) {
        return null
      } else {
        uniqSites.add(name.replace('www.', ''))

        return [ ip, cleanedName ]
      }
    }).filter(Boolean)

    // Output all of the domains in a nice way
    lines.forEach(([ ip, name ]) => {
      process.stdout.write(`${name}: ${ip}\n`)
    })
  })
  .help('h')
  .argv
